from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import numpy as np
from pretraining_load import *

stepsNum = 2000

size = portions_num*dim


feature_columns = [tf.contrib.layers.real_valued_column("", dimension=150)]
print(feature_columns)
classifier = tf.contrib.learn.DNNClassifier(feature_columns=feature_columns, hidden_units=[10, 20, 10], n_classes=3, model_dir="/tmp/test_model")
classifier.fit(x=preTrX, y=preTrY, steps=stepsNum)

accuracy_score = classifier.evaluate(x=preTestX, y=preTestY)["accuracy"]
print('Accuracy: {0:f}'.format(accuracy_score))
