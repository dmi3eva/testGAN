import tensorflow as tf
import numpy as np

userNum = 1
notUserNum = 2
numOfExp = 16
dim = 9
maxSize = 10000
bSize = 2000
genLSize = 128
batch_size = 3
dL2Size = 100
capacity = 5 
min_after_dequeue = 3
epochs = 1000
k = 5

def readData():
    data = np.zeros((numOfExp * 2, maxSize,dim))
    res = np.zeros((numOfExp * 2, maxSize))

    data_size = np.zeros((numOfExp * 2))
    for i in range(1,numOfExp):
        fileName = "users/user"  + str(userNum) + "/w" + str(i) + ".csv"
        x_data = np.loadtxt(fileName,delimiter=",",skiprows=1)
        l = x_data.shape
        data_size[i] = l[0]
        x_data = np.resize(x_data, (maxSize,  dim))
        data[i] = x_data
        tmp = np.ones(maxSize)
        res[i] = tmp
    
    
    for i in range(1,numOfExp):
        fileName = "users/user"  + str(notUserNum) + "/w" + str(i) + ".csv"
        x_data = np.loadtxt(fileName,delimiter=",",skiprows=1)
        l = x_data.shape
        data_size[i] = l[0]
        x_data = np.resize(x_data, (maxSize,  dim))
        data[i + numOfExp - 1] = x_data
        tmp = np.zeros(maxSize)
        res[i] = tmp
    data.resize(numOfExp * 2, bSize,dim)
    example_batch, label_batch = tf.train.shuffle_batch([data, res], batch_size = batch_size, capacity = capacity,
      min_after_dequeue = min_after_dequeue)
    return example_batch, label_batch

    
   
def get_weights(shape):
    return tf.Variable(tf.truncated_normal(shape=shape, stddev=0.01))

def get_bias(shape):
    return tf.Variable(tf.constant(shape=shape, value=0.01))
    

def generator(X):
    with tf.name_scope('G'):
        g_l1_size, g_l2_size = genLSize, genLSize
        g_w1 = get_weights([bSize * dim, g_l1_size])
        g_b1 = get_bias([g_l1_size])
        g_w2 = get_weights([g_l1_size, g_l2_size])
        g_b2 = get_bias([g_l2_size])
        g_w3 = get_weights([g_l2_size, bSize * dim])
        g_b3 = get_bias([bSize * dim])
        l1 = tf.nn.relu(tf.add(tf.matmul(X, g_w1), g_b1))
        l2 = tf.nn.relu(tf.add(tf.matmul(l1, g_w2), g_b2))
        g_res = tf.sigmoid(tf.add(tf.matmul(l2, g_w3), g_b3))
        return g_res
        
class Discriminator():
    def __init__(self):
        with tf.name_scope('D'):
            d_l1_size, d_l2_size = dL1Size, dL2Size
            self.d_w1 = get_weights([bSize * dim, d_l1_size])
            self.d_b1 = get_bias([d_l1_size])
            self.d_w2 = get_weights([d_l1_size, d_l2_size])
            self.d_b2 = get_bias([d_l2_size])
            self.d_w3 = get_weights([d_l2_size, 1])
            self.d_b3 = get_bias([1])
            
    def network(self, d_input):
        d_l1 = tf.nn.dropout(tf.nn.relu(tf.add(tf.matmul(d_input, self.d_w1), self.d_b1)), 0.5)
        d_l2 = tf.nn.dropout(tf.nn.relu(tf.add(tf.matmul(d_l1, self.d_w2), self.d_b2)), 0.5)
        d_res = tf.sigmoid(tf.add(tf.matmul(d_l2, self.d_w3), self.d_b3))
        return d_res
        
        

example_batch, label_batch = readData()
X = tf.placeholder(shape=[None, bSize * dim], dtype=tf.float32)
Z = tf.placeholder(shape=[None, bSize * dim], dtype=tf.float32)

g_z = generator(Z)
disc = Discriminator()
d_x = disc.network(X)
d_z = disc.network(g_z)

g_batch = tf.Variable(0)
d_batch = tf.Variable(0)

g_loss = - tf.reduce_mean(tf.reduce_sum(tf.log(d_z), reduction_indices = [1]))
g_learning_rate = tf.train.exponential_decay(0.01, g_batch, 100, 0.95, staircase=True)
g_optimizer = tf.train.MomentumOptimizer(g_learning_rate, 0.9).minimize(g_loss)

d_loss = - tf.reduce_mean(tf.reduce_sum(tf.log(d_x) + tf.log(1-d_z), reduction_indices=[1]))
d_learning_rate = tf.train.exponential_decay(0.01, g_batch, 100, 0.95, staircase=True)
d_optimizer = tf.train.MomentumOptimizer(d_learning_rate, 0.9).minimize(d_loss)


with tf.Session() as sess:
    sess.run(tf.initialize_all_variables())
    for i in range(epochs):
        for j in range(k):
            batch_z = np.random.normal(0, 0.1, (batch_size,bSize * dim))
            batch_x = example_batch.train.next_batch(batch_size)[0]
            _, d_l = sess.run([d_optimizer, d_loss], feed_dict={X:batch_x, Z:batch_z})
            
        batch_z = np.random.normal(0, 0.1, batch_size*28*28).reshape((batch_size, bSize * dim))
        batch_x = example_batch.train.next_batch(batch_size)[0]
        _t, g_l = sess.run([g_optimizer, g_loss], feed_dict={X:batch_x, Z:batch_z})
        if i%100 ==0:
            print ("loss:%f"%d_l)
