import tensorflow as tf
import numpy as np

#notUser_num = 2

user_num = 1
files_num = 15 #_numOfExp
dim = 3
portionSize = 50

portionsMax = 90000

validation_percentage = 25 
test_percentage = 25
false_percentage = 30
train_percentage = 100 - validation_percentage - false_percentage
train_num  = 0

bSize = 2000
genLSize = 128
batch_size = 3
dL2Size = 100
capacity = 5 
min_after_dequeue = 3
epochs = 1000
k = 5

def read_data():    
    data = np.zeros((portionsMax, portionSize * dim))
    portions_num = 0
    last_num = 0
    for i in range(1, files_num):
        fileName = "users/user"  + str(user_num) + "/w" + str(i) + ".csv"
        x_data = np.loadtxt(fileName, usecols = (1, 2, 3), delimiter=",",skiprows=1)
        l = x_data.shape
        
        portionsInFile = l[0] / portionSize
        portions_num += portionsInFile
        for j in range (portionsInFile):
            x_data_part = x_data[j * portionSize : (j + 1) * portionSize,]
            x_data_part = np.resize(x_data_part, portionSize * dim)
            data[last_num] = x_data_part
            last_num = last_num + 1
    data.resize(portions_num, portionSize * dim) 
      
    return data, portions_num
    
def sign_data(portions_num):
    train_num = (portions_num * train_percentage) / 100
    false_num = (train_num * false_percentage) / 100
    res = np.ones(portions_num)
    for i in range(false_num):
        res[i] = 0
    return res

def devide_data(data, res, portions_num):
    train_num = (portions_num * train_percentage) / 100
    val_num = (portions_num * validation_percentage) / 100
    test_num = portions_num - train_num - val_num
    train_data = data[0 : train_num,]
    train_res = res[0 : train_num,]
    
    val_data = data[train_num : train_num + val_num,]
    val_res = res[train_num : train_num + val_num,]
    test_data = data[train_num + val_num : portions_num,]
    test_res = res[train_num + val_num : portions_num,]
    trX = train_data.reshape((train_num,dim * portionSize)).astype(float)
    trY = train_res.reshape((train_num)).astype(float)
    valX = val_data.reshape((val_num,dim * portionSize)).astype(float)
    valY = val_res.reshape((val_num)).astype(float)
    testX = test_data.reshape((test_num,dim * portionSize)).astype(float)
    testY = test_res.reshape((test_num)).astype(float)
    return trX, trY, valX, valY, testX, testY
    
example, portions_num = read_data()
label = sign_data(portions_num)
trX, trY, valX, valY, testX, testY = devide_data(example, label, portions_num)


print(portions_num)